<h1>Pequeno-Sistema-de-Vendas-Java</h1>
<p>Aplicação desenvolvida como material de apoio ao aprendizado de programação Java usando o pacote Swing. É fornecida gratuitamente.</p>
<p>Softwares utilizados:
  <ul>
    <li>JDK - 11</li>
    <li>NetBeans IDE 8.2</li>
    <li>MySQL Server 5.1.53-community</li>
    <li>MySQL Connector JDBC 5.1.23</li>
    <li>MySQL Workbench</li> 
    <li>Windows 10</li>
    <li>Enterprise Architect Trial</li>
  </ul>
</p>
<h3> Acesso ao Sistema</h3>
<p><b>Usuario: adm</b></p>
<p><b>Senha: 123</b></p>

<p>Pré Visualização</p>
<a>
    <img src="src/com/jdenner/gui/img/menu.png">
    <img src="src/com/jdenner/gui/img/login.png">
    <img src="src/com/jdenner/gui/img/menu%20venda.png">
</a>

<p>Esses foi meu primeiro programa java - Não irei continua pois estou fazendo um outro bem mais elaborado em Web.</p>

<p>mais informações</p>
<p>https://www.dropbox.com/sh/sweyvj0s473csr2/AABmtQLhN_TcdRexHPXy9HBRa?dl=0</p>
